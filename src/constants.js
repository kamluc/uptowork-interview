const defaultData = {
  name: 'Your name',
  summary: 'Place for your Summary',
  sections: []
};

const mockData = {
  name: 'Jane Doe1',
  summary: 'Small Business Operations Management, Freelance Writer',
  sections: [{
    type: 'text',
    title: 'About',
    value: `Kristin Kaltman and Employ Resume proudly partner with Peak Performance Life and Business Coach,
    Diego Miranda. Coach Diego specializes in coaching to improve our clients' business strategies,
    health, fitness, vitality, and overall quality of life. For more information on Coach Diego
    Miranda, please visit his website at www.coachdiegomiranda.com. Employ Resume is also a proud
    referral partner of Cake Resume, an innovative online resume-building platform. Employ encourages
    all of their clients to increase their professional online presence in this new digital era by
    maintaining updated professional social media accounts.`
  }, {
    type: 'list',
    title: 'Experience',
    items: [{
      title: 'Software Engineer1',
      timeframe: '2010-14',
      description: 'Bla bla bla'
    }, {
      title: 'Software Engineer2',
      timeframe: '2014-2018',
      description: 'La la la'
    }]
  }]
};

export { defaultData, mockData };