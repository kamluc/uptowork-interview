import React from 'react';
import './App.css';
import _ from 'lodash';
import SectionItem from './SectionItem';

const SectionContainer = ({ form, addSection, handleChange }) => <div className='row mt-4'>
  <div className='col'>
    <div className='add-section d-flex flex-row-reverse'>
      <button className="btn btn-primary" onClick={addSection}>Add Section</button>
    </div>
    {_.get(form, 'values.sections', [])
      .map((section, idx) => <SectionItem key={idx} sectionIndex={idx} form={form}
                                          handleChange={handleChange} {...section}/>)}
  </div>
</div>;

export default SectionContainer;
