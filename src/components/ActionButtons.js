import React from 'react';

export const ActionButtons = ({ fetchMockData }) => <div className='row mb-3'>
  <div className='col d-flex flex-row-reverse'>
    <button className="btn btn-secondary" onClick={fetchMockData}>Load defaults</button>
  </div>
</div>;
