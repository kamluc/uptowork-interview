import React, { Component } from 'react';
import './EditableElement.css';
import _ from 'lodash';
import { getLastInPath } from '../helpers';

class EditableElement extends Component {
  constructor(props) {
    super(props);
    this.onValueChange = this.onValueChange.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.state = {
      isEditing: false,
      value: _.get(props.values, props.path ? props.path : props.name, '')
    };
  }

  handleEdit() {
    this.setState({ isEditing: !this.state.isEditing });
  }

  onValueChange(e) {
    const { handleChange } = this.props;
    this.setState({ value: e.target.value });
    handleChange(e);
  }

  render() {
    const { isEditing, value } = this.state;
    const { name, path } = this.props;

    const inputName = path ? getLastInPath(path) : name;

    if (isEditing) {
      return <span>
          <input name={inputName} value={this.state.value} onChange={this.onValueChange}/>
        <button type='button' onClick={this.handleEdit}>Save</button>
        </span>;
    } else {
      return <span className='editable' onDoubleClick={this.handleEdit}>{value}</span>;
    }
  }
}

export default EditableElement;