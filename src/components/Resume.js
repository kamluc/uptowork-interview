import React from 'react';
import './Resume.css';
import { defaultData, mockData } from '../constants';

class Resume extends React.Component {
  constructor(props) {
    super(props);
    this.addSection = this.addSection.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.fetchMockData = this.fetchMockData.bind(this);
    this.state = {
      isFetching: false,
      form: {
        values: defaultData
      }
    };
  }

  handleChange(event) {
    this.setState({
      form: {
        ...this.state.form,
        values: {
          ...this.state.form.values,
          [event.target.name]: event.target.value
        }
      }
    });
  }

  fetchMockData() {
    this.setState({ isFetching: true });
    setTimeout(() => {
      this.setState({
        isFetching: false,
        form: {
          ...this.state.form,
          values: mockData
        }
      });
    }, 1500);
  }

  addSection() {
    const { sections } = this.state.form.values;
    sections.push({
      type: 'text',
      title: 'Section title',
      value: 'Section description'
    });
    this.setState({
      form: {
        ...this.state.form,
        values: {
          ...this.state.form.values,
          sections
        }
      }
    });
  }

  render() {
    const { children } = this.props;
    const { form, isFetching } = this.state;
    const childrenWithProps = React.Children.map(children, child =>
      React.cloneElement(child, {
        form,
        handleChange: this.handleChange,
        addSection: this.addSection,
        fetchMockData: this.fetchMockData
      }));

    return <div className='Resume'>
      {!isFetching && childrenWithProps}
      {isFetching && <span>Loading...</span>}
    </div>;
  }
}

export default Resume;