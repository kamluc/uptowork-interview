import React from 'react';
import EditableElement from './EditableElement';

const ListSection = ({ items, form, handleChange, sectionIndex }) => {
  return <section className='mt-3 mb-3'>
    <h3>
      <EditableElement {...form} path={`sections[${sectionIndex}].title`} handleChange={handleChange}/>
    </h3>
    {items.map((item, idx) => <li key={idx} className="list-group-item">
      <div>
      <span className='title'>
          <EditableElement {...form}
                           path={`sections[${sectionIndex}].items[${idx}].title`}
                           handleChange={handleChange}/>
      </span>
        <span className='timeframe float-right'>
              <EditableElement {...form}
                               path={`sections[${sectionIndex}].items[${idx}].timeframe`}
                               handleChange={handleChange}/>
      </span>
        <div>
          <EditableElement {...form}
                           path={`sections[${sectionIndex}].items[${idx}].description`}
                           handleChange={handleChange}/>
        </div>
      </div>
    </li>)}
  </section>;
};

export default ListSection;