import React from 'react';
import './Picture.css';
import picture from './picture.png';

const Picture = () => <div className='Picture row text-center mt-3'>
  <div className='col'>
    <img src={picture} alt=""/>
  </div>
</div>;

export default Picture;
