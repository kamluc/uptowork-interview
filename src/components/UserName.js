import React from 'react';
import './App.css';
import EditableElement from './EditableElement';

const UserName = ({ form, handleChange }) => <div className='row text-center mt-3'>
  <div className='col'>
    <p className='name'>
      <EditableElement {...form} name='name' handleChange={handleChange}/>
    </p>
  </div>
</div>;

export default UserName;
