import React, { Component } from 'react';
import './SectionItem.css';
import ListSection from './ListSection';
import TextSection from './TextSection';

class SectionItem extends Component {
  render() {
    const { type } = this.props;

    console.log(this.props)

    if (type === 'text') {
      return <TextSection {...this.props}/>;
    }
    if (type === 'list') {
      return <ListSection {...this.props}/>;
    }
    if (!type) {
      return <div>Place for new section</div>;
    }
  }
}

export default SectionItem;
