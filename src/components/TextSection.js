import React from 'react';
import EditableElement from './EditableElement';

const TextSection = ({ form, handleChange, sectionIndex }) => <section className='mt-3 mb-3'>
  <h3>
    <EditableElement {...form} path={`sections[${sectionIndex}].title`} handleChange={handleChange}/>
  </h3>
  <p>
    <EditableElement {...form} path={`sections[${sectionIndex}].value`} handleChange={handleChange}/>
  </p>
</section>;

export default TextSection;