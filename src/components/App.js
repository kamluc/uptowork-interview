import React from 'react';
import './App.css';
import Container from './Container';
import Resume from './Resume';
import Picture from './Picture';
import UserName from './UserName';
import UserSummary from './UserSummary';
import { ActionButtons } from './ActionButtons';
import SectionContainer from './SectionContainer';

const App = () =>
  <div className='App'>
    <Container>
      <Resume>
        <ActionButtons/>
        <Picture/>
        <UserName/>
        <UserSummary/>
        <SectionContainer/>
      </Resume>
    </Container>
  </div>;

export default App;
