import React from 'react';
import './UserSummary.css';
import EditableElement from './EditableElement';

const UserSummary = ({ form, handleChange }) => <div className='row text-center'>
  <div className='col'>
    <div className='summary'>
      <p>
        <EditableElement {...form} name='summary' handleChange={handleChange}/>
      </p>
    </div>
  </div>
</div>;

export default UserSummary;