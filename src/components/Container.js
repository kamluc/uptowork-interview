import React from 'react';
import './App.css';

const Container = ({ children }) => <div className='container'>
  {children}
</div>;

export default Container;