/*
 *  Returns last string in object path with dots.
 *
 *  Example:
 *  `aa.bb[1].cc` returns `cc`
 */
export const getLastInPath = path => {
  const LAST_ELEMENT_IN_STING_PATH_REGEX = /(?:\.\S+)?\.(\w+)/;
  const result = LAST_ELEMENT_IN_STING_PATH_REGEX.exec(path);
  if (Array.isArray(result)) {
    return result[1];
  } else {
    return null;
  }
};